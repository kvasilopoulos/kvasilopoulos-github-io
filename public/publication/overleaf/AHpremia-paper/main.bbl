\ifx\undefined\bysame
\newcommand{\bysame}{\leavevmode\hbox to\leftmargin{\hrulefill\,\,}}
\fi
\begin{thebibliography}{xx}

\harvarditem[Amihud and Mendelson]{Amihud and Mendelson}{1986}{amihudM1986}
Amihud, Yakov, and Haim Mendelson (1986) `Asset pricing and the bid-ask
  spread.' {\it Journal of Financial Economics} 17(2),~223--249

\harvarditem[Blanchard]{Blanchard}{1979}{Blanchard1979}
Blanchard, Olivier~J. (1979) `Speculative bubbles, crashes and rational
  expectations.' {\it Economics Letters} 3(4),~387--389

\harvarditem[Cai et al.]{Cai, {McGuinness} and Zhang}{2011}{caiMZ2011}
Cai, Charlie~X, Paul~B {McGuinness}, and Qi~Zhang (2011) `{The pricing dynamics
  of cross-listed securities: The case of Chinese A-and H-shares}.' {\it
  Journal of Banking \& Finance} 35(8),~2123--2136

\harvarditem[Campbell and Yogo]{Campbell and Yogo}{2006}{CampbellY2006}
Campbell, John~Y., and Motohiro Yogo (2006) `Efficient tests of stock return
  predictability.' {\it Journal of Financial Economics} 81(1),~27--60

\harvarditem[Carpenter and Whitelaw]{Carpenter and
  Whitelaw}{2017}{carpenterW2017}
Carpenter, Jennifer~N, and Robert~F Whitelaw (2017) `The development of
  {C}hina's stock market and stakes for the global economy.' {\it Annual Review
  of Financial Economics} 9,~233--257

\harvarditem[Chan et al.]{Chan, Menkveld and Yang}{2008}{chanMY2008}
Chan, Kalok, Albert~J Menkveld, and Zhishu Yang (2008) `{Information asymmetry
  and asset prices: Evidence from the China foreign share discount}.' {\it The
  Journal of Finance} 63(1),~159--196

\harvarditem[Chang]{Chang}{2004}{chang2004}
Chang, Yoosoon (2004) `Bootstrap unit root tests in panels with
  cross--sectional dependency.' {\it Journal of econometrics} 120(2),~263--293

\harvarditem[Chen and Wen]{Chen and Wen}{2017}{chenW2017}
Chen, Kaiji, and Yi~Wen (2017) `{The great housing boom of China}.' {\it
  American Economic Journal: Macroeconomics} 9(2),~73--114

\harvarditem[Chen and Knez]{Chen and Knez}{1995}{chenK1995}
Chen, Zhiwu, and Peter~J. Knez (1995) `Measurement of market integration and
  arbitrage.' {\it The Review of Financial Studies} 8(2),~287--325

\harvarditem[Chung et al.]{Chung, Hui and Li}{2013}{chungHL2013}
Chung, Tsz-Kin, Cho-Hoi Hui, and Ka-Fai Li (2013) `{Explaining share price
  disparity with parameter uncertainty: Evidence from Chinese A-and H-shares}.'
  {\it Journal of Banking \& Finance} 37(3),~1073--1083

\harvarditem[Diba and Grossman]{Diba and Grossman}{1988}{DibaG1988}
Diba, Behzad~T., and Herschel~I. Grossman (1988) `The theory of rational
  bubbles in stock prices.' {\it Economic Journal} 98(392),~746--54

\harvarditem[Elliott et al.]{Elliott, M\"{u}ller and
  Watson}{2015}{ElliotMS2015}
Elliott, Graham, Ulrich~K. M\"{u}ller, and Mark~W. Watson (2015) `Nearly
  optimal tests when a nuisance parameter is present under the null
  hypothesis.' {\it Econometrica} 83(2),~771--811

\harvarditem[Evans]{Evans}{1991}{Evans1991}
Evans, George~W. (1991) `Pitfalls in testing for explosive bubbles in asset
  prices.' {\it American Economic Review} 81(4),~922--930

\harvarditem[Fang et al.]{Fang, Gu, Xiong and Zhou}{2016}{fangGXZ2016}
Fang, Hanming, Quanlin Gu, Wei Xiong, and Li-An Zhou (2016) `{Demystifying the
  Chinese housing boom}.' {\it NBER macroeconomics annual} 30(1),~105--166

\harvarditem[Feng and Seasholes]{Feng and Seasholes}{2003}{fengS2003}
Feng, Lei, and Mark Seasholes (2003) `A profile of individual investors in an
  emerging stock market.' Working Paper

\harvarditem[Fernald and Rogers]{Fernald and Rogers}{2002}{fernaldR2002}
Fernald, John, and John~H. Rogers (2002) `{Puzzles in the Chinese stock
  market}.' {\it Review of Economics and Statistics} 84(3),~416--432

\harvarditem[Flood and Garber]{Flood and Garber}{1994}{floodG1994}
Flood, Robert~P., and Peter~M. Garber (1994) {\it Speculative bubbles,
  speculative attacks, and policy switching} (MIT Press)

\harvarditem[Froot and Dabora]{Froot and Dabora}{1999}{frootD1999}
Froot, Kenneth~A., and Emil~M. Dabora (1999) `How are stock prices affected by
  the location of trade?' {\it Journal of Financial Economics} 53(2),~189 --
  216

\harvarditem[Gan et al.]{Gan, Yin, Jia, Xu, Ma and Zheng}{2014}{ganYJXMZ2014}
Gan, Li, Zhichao Yin, Nan Jia, Shu Xu, Shuang Ma, and Lu~Zheng (2014) {\it Data
  you need to know about China} (Springer)

\harvarditem[Glaeser et al.]{Glaeser, Huang, Ma and
  Shleifer}{2017}{glaeserHMS2017}
Glaeser, Edward, Wei Huang, Yueran Ma, and Andrei Shleifer (2017) `{A real
  estate boom with Chinese characteristics}.' {\it Journal of Economic
  Perspectives} 31(1),~93--116

\harvarditem[G\"{u}rkaynak]{G\"{u}rkaynak}{2008}{Gurkaynak2008}
G\"{u}rkaynak, Refet~S. (2008) `Econometric tests of asset price bubbles:
  Taking stock.' {\it Journal of Economic Surveys} 22(1),~166--186

\harvarditem[Hamilton and Whiteman]{Hamilton and Whiteman}{1985}{HamiltonW1985}
Hamilton, James~D., and Charles~H. Whiteman (1985) `The observable implications
  of self-fulfilling expectations.' {\it Journal of Monetary Economics}
  16(3),~353--373

\harvarditem[Hausman]{Hausman}{2001}{Hausman2001}
Hausman, Jerry (2001) `Mismeasured variables in econometric analysis: Problems
  from the right and problems from the left.' {\it The Journal of Economic
  Perspectives} 15(4),~57--67

\harvarditem[Homm and Breitung]{Homm and Breitung}{2012}{HommB2012}
Homm, Ulrich, and J{\"o}rg Breitung (2012) `Testing for speculative bubbles in
  stock markets: {A} comparison of alternative methods.' {\it Journal of
  Financial Econometrics} 10(1),~198--231

\harvarditem[Im et al.]{Im, Pesaran and Shin}{2003}{imPS2003}
Im, Kyung~So, M~Hashem Pesaran, and Yongcheol Shin (2003) `Testing for unit
  roots in heterogeneous panels.' {\it Journal of econometrics} 115(1),~53--74

\harvarditem[Jansson and Moreira]{Jansson and Moreira}{2006}{JanssonM2006}
Jansson, Michael, and Marcelo~J. Moreira (2006) `Optimal inference in
  regression models with nearly integrated regressors.' {\it Econometrica}
  74(3),~681--714

\harvarditem[Jia et al.]{Jia, Wang and Xiong}{2017}{jiaWX2017}
Jia, Chunxin, Yaping Wang, and Wei Xiong (2017) `Market segmentation and
  differential reactions of local and foreign investors to analyst
  recommendations.' {\it The Review of Financial Studies} 30(9),~2972--3008

\harvarditem[Kilian]{Kilian}{1999}{kilian1999}
Kilian, Lutz (1999) `Exchange rates and monetary fundamentals: What do we learn
  from long-horizon regressions?' {\it Journal of Applied Econometrics}
  14(5),~491--510

\harvarditem[Kilian and Taylor]{Kilian and Taylor}{2003}{kilianT2003}
Kilian, Lutz, and Mark~P. Taylor (2003) `{Why is it so difficult to beat the
  random walk forecast of exchange rates?}' {\it Journal of International
  Economics} 60(1),~85--107

\harvarditem[Kostakis et al.]{Kostakis, Magdalinos and
  Stamatogiannis}{2015}{KostakisMS2015}
Kostakis, Alexandros, Tassos Magdalinos, and Michalis~P. Stamatogiannis (2015)
  `Robust econometric inference for stock return predictability.' {\it Review
  of Financial Studies} 28(5),~1506--1553

\harvarditem[Lamont and Thaler]{Lamont and Thaler}{2003}{LamontT2003}
Lamont, Owen~A., and Richard~H. Thaler (2003) `Anomalies: The law of one price
  in financial markets.' {\it Journal of Economic Perspectives} 17(4),~191--202

\harvarditem[Liu et al.]{Liu, Zhang and Zhao}{2014}{liuZZ2014}
Liu, Yu-Jane, Zheng Zhang, and Longkai Zhao (2014) `Speculation spillovers.'
  {\it Management Science} 61(3),~649--664

\harvarditem[Ma]{Ma}{1996}{Ma1996}
Ma, Xianghai (1996) `{Capital controls, market segmentation and stock prices:
  Evidence from the Chinese stock market}.' {\it Pacific-Basin Finance Journal}
  4(2-3),~219--239

\harvarditem[Maddala and Wu]{Maddala and Wu}{1999}{maddala1999}
Maddala, Gangadharrao~S, and Shaowen Wu (1999) `A comparative study of unit
  root tests with panel data and a new simple test.' {\it Oxford Bulletin of
  Economics and statistics} 61(S1),~631--652

\harvarditem[Mao and Shen]{Mao and Shen}{2019}{maoS2019}
Mao, Guangyu, and Yan Shen (2019) `{Bubbles or fundamentals? Modeling
  provincial house prices in China allowing for cross-sectional dependence}.'
  {\it China Economic Review} 53,~53--64

\harvarditem[Mei et al.]{Mei, Scheinkman and Xiong}{2005}{meiSX2005}
Mei, Jianping, Jose Scheinkman, and Wei Xiong (2005) `{Speculative trading and
  stock prices: Evidence from Chinese AB share premia}.' Technical Report,
  National Bureau of Economic Research

\harvarditem[Nielsen]{Nielsen}{2010}{nielsen2010}
Nielsen, Bent (2010) `Analysis of coexplosive processes.' {\it Econometric
  Theory} 26(3),~882--915

\harvarditem[Pavlidis et al.]{Pavlidis, Yusupova, Paya, Peel,
  Mart{\'\i}nez-Garc{\'\i}a, Mack and Grossman}{2016}{pavlidis2016}
Pavlidis, Efthymios~G., Alisa Yusupova, Ivan Paya, David~A. Peel, Enrique
  Mart{\'\i}nez-Garc{\'\i}a, Adrienne Mack, and Valerie Grossman (2016)
  `Episodes of exuberance in housing markets: In search of the smoking gun.'
  {\it The Journal of Real Estate Finance and Economics} 53(4),~419--449

\harvarditem[Pavlidis et al.]{Pavlidis, Martinez-Garcia and
  Grossman}{2019}{pavlidisMG2019}
Pavlidis, Efthymios~G., Enrique Martinez-Garcia, and Valerie Grossman (2019)
  `Detecting periods of exuberance: A look at the role of aggregation with an
  application to house prices.' {\it Economic Modelling} 80,~87--102

\harvarditem[Pavlidis et al.]{Pavlidis, Paya and Peel}{2017}{pavlidisPPf2017}
Pavlidis, Efthymios~G., Ivan Paya, and David~A. Peel (2017) `Testing for
  speculative bubbles using spot and forward prices.' {\it International
  Economic Review} 58(4),~1191--1226

\harvarditem[Pavlidis et al.]{Pavlidis, Paya and Peel}{2018}{pavlidisPP2018}
Pavlidis, Efthymios~G., Ivan Paya, and David~A. Peel (2018) `Using market
  expectations to test for speculative bubbles in the crude oil market.' {\it
  Journal of Money, Credit and Banking} 50(5),~833--856

\harvarditem[Phillips]{Phillips}{2014}{Phillips2014}
Phillips, Peter C.~B. (2014) `On confidence intervals for autoregressive roots
  and predictive regression.' {\it Econometrica} 82(3),~1177--1195

\harvarditem[Phillips and Lee]{Phillips and Lee}{2013}{PhillipsL2013}
Phillips, Peter C.~B., and Ji~Hyung Lee (2013) `Predictive regression under
  various degrees of persistence and robust long-horizon regression.' {\it
  Journal of Econometrics} 177(2),~250--264

\harvarditem[Phillips and Magdalinos]{Phillips and
  Magdalinos}{2009}{PhillipsM2009}
Phillips, Peter C.~B., and Tassos Magdalinos (2009) `Econometric inference in
  the vicinity of unity.' CoFie Working Paper~7, Singapore Management
  University

\harvarditem[Phillips et al.]{Phillips, Shi and Yu}{2015a}{PhillipsSY2015a}
Phillips, Peter C.~B., Shuping Shi, and Jun Yu (2015a) `Testing for multiple
  bubbles: Historical episodes of exuberance and collapse in the {S\&P} 500.'
  {\it International Economic Review} 56(4),~1043--1078

\harvarditem[Phillips et al.]{Phillips, Shi and Yu}{2015b}{PhillipsSY2015b}
\bysame (2015b) `Testing for multiple bubbles: {Limit} theory of real-time
  detectors.' {\it International Economic Review} 56,~1079--1134

\harvarditem[Scheinkman and Xiong]{Scheinkman and Xiong}{2003}{ScheinkmanX2003}
Scheinkman, José~A., and Wei Xiong (2003) `Overconfidence and speculative
  bubbles.' {\it Journal of Political Economy} 111(6),~1183--1220

\harvarditem[Scheinkman]{Scheinkman}{2014}{scheinkman2014}
Scheinkman, Jose~A. (2014) {\it Speculation, trading, and bubbles} (Columbia
  University Press)

\harvarditem[Seasholes and Liu]{Seasholes and Liu}{2011}{seasholesL2011}
Seasholes, Mark~S., and Clark Liu (2011) `Trading imbalances and the law of one
  price.' {\it Economics Letters} 112(1),~132--134

\harvarditem[Stambaugh et al.]{Stambaugh, Yu and Yuan}{2012}{stambaughYY2012}
Stambaugh, Robert~F., Jianfeng Yu, and Yu~Yuan (2012) `The short of it:
  Investor sentiment and anomalies.' {\it Journal of Financial Economics}
  104(2),~288--302

\harvarditem[Wang and Jiang]{Wang and Jiang}{2004}{wangJ2004}
Wang, Steven~Shuye, and Li~Jiang (2004) `{Location of trade, ownership
  restrictions, and market illiquidity: Examining Chinese A-and H-shares}.'
  {\it Journal of Banking \& Finance} 28(6),~1273--1297

\harvarditem[Wooldridge]{Wooldridge}{2001}{wooldridge2001}
Wooldridge, Jeffrey~M. (2001) {\it Econometric analysis of cross section and
  panel data,} vol.~1 of {\em MIT Press Books} (The MIT Press)

\harvarditem[Wu et al.]{Wu, Gyourko and Deng}{2015}{wuGD2015}
Wu, Jing, Joseph Gyourko, and Yongheng Deng (2015) `{Real estate collateral
  value and investment: The case of China}.' {\it Journal of urban Economics}
  86,~43--53

\harvarditem[Xiong and Yu]{Xiong and Yu}{2011}{XiongY2011}
Xiong, Wei, and Jialin Yu (2011) `{The Chinese warrants bubble}.' {\it American
  Economic Review} 101(6),~2723--53

\harvarditem[Zhi et al.]{Zhi, Li, Jiang, Wei and Sornette}{2019}{zhiLJWS2019}
Zhi, Tianhao, Zhongfei Li, Zhiqiang Jiang, Lijian Wei, and Didier Sornette
  (2019) `{Is there a housing bubble in China?}' {\it Emerging Markets Review}
  39,~120--132

\end{thebibliography}
