+++
# About/Biography widget.
widget = "about"
active = true
date = 2016-04-20T00:00:00

# Order that this section will appear in.
weight = 1

# List your academic interests.
[interests]
  interests = [
    "Applied Macroeconomics",
    "Time Series Econometrics",
    "Explosive Dynamics",
    "Real Estate Markets",
    "Credit Frictions",
    "Dynamic Stochastic General Equilibrium (DSGE) modelling"
  ]

# List your qualifications (such as academic degrees).

[[education.courses]]
  course = "Ph.D. Candidate in Economics"
  institution = "Lancaster University"
  year = 2016

[[education.courses]]
  course = "MSc Economics"
  institution = "University of Macedonia (Greece)"
  year = 2014

[[education.courses]]
  course = "BSc Economics"
  institution = "University of Macedonia (Greece)"
  year = 2009
 
+++

# Welcome !
 
My name is Kostas, I am a macroeconomist and PhD candidate in Economics at Lancaster University. My research interests include theoretical and applied macroeconomics focused on the real estate markets. In particular, I examine the part of Commercial Real Estate in the economy and its interaction with the Housing market.