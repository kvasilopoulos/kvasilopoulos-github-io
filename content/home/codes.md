+++
# Custom widget.
# An example of using the custom widget to create your own homepage section.
# To create more sections, duplicate this file and edit the values below as desired.
widget = "custom"
active = true
date = "2016-04-20T00:00:00"

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Codes"
subtitle = "Selected R packages"

# Order that this section will appear in.
weight = 60
+++

 
<p style = "font-size:18px;margin-left:-2em; font-weight: bold; margin-bottom: 2rem;"> 
  I am the author and the maintainer of the following packages on CRAN: 
</p>


<!-- Statistical Packages -->

<!--<p style = "font-size:22px;font-weight: bold;margin-left:-2em; margin-bottom: 1rem;"> 
  Statistical Packages
</p>
-->

<!-- exuber -->

<h4 style="padding-bottom:1em;">
  <i class="fas fa-cube"></i>
  <a href = "https://kvasilopoulos.github.io/exuber/" target="_blank">
    &nbsp; exuber : Econometric Analysis of Explosive Time Series 
  </a>
</h4>

<!-- ivx -->

<h4 style="padding-bottom:1em;">
  <i class="fas fa-cube"></i>
  <a href = "https://kvasilopoulos.github.io/ivx/" target="_blank">
    &nbsp; ivx : Robust Econometric Inference
  </a>
</h4>


<!-- ihpdr -->

<h4>
  <i class="fas fa-cube"></i> 
  <a href = "https://https://github.com/kvasilopoulos/uklr" target="_blank">
    &nbsp; uklr: Client to United Kingdom Land Registry
  </a>
</h4>


<!-- Rest -->

<p style = "font-size:18px; margin-left:-2em; font-weight: bold; margin-top: 2rem;"> 
  For a comprehensive list you can see 
  <a href="./codes/"> here. </a>
</p>


<!-- > Simulate a variety of periodically-collapsing bubble models. The estimation
> and simulation utilize the matrix inversion lemma from the recursive least squares
> algorithm, which results in a significant speed improvement. -->
