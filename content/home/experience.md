+++
# Experience widget.
widget = "experience"  # Do not modify this line!
active = true  # Activate this widget? true/false

title = "Experience"
subtitle = ""

# Order that this section will appear in.
weight = 8

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "January 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Research Assistant"
  company = "Lancaster University - UK Housing Observatory"
  company_url = "https://www.lancaster.ac.uk/lums/our-departments/economics/research/uk-housing-observatory/"
  location = "Lancaster, United Kingdom"
  date_start = "2019-03-01"
  date_end = ""
  description = """ """
  
[[experience]]
  title = "Postgraduate Teaching Assistant"
  company = "Lancaster University"
  company_url = "https://www.lancaster.ac.uk/"
  location = "Lancaster, United Kingdom"
  date_start = "2016-09-01"
  date_end = ""
  description = """ 
  Courses Tutored:
  
  * [Econ222: Intermediate Macroeconomics I](http://www.lusi.lancaster.ac.uk/CoursesHandbook/ModuleDetails/ModuleDetail?yearId=000114&courseId=016985)
  * [Econ 102: Principles of Economics B](http://www.lusi.lancaster.ac.uk/CoursesHandbook/ModuleDetails/ModuleDetail?yearId=000114&courseId=018424)

"""


+++
