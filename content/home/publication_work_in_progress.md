+++
# Recent Publications widget.
# This widget displays recent publications from `content/publication/`.
widget = "publications"
active = true
date = 2016-04-20

title = "Work in Progress"
subtitle = ""

# Order that this section will appear in.
weight = 30

# Number of publications to list.
count = 30

# List format.
#   0 = Simple
#   1 = Detailed
#   2 = APA
#   3 = MLA
list_format = 2


# Filter by publication type.
# -1: Any
# 'Uncategorized',  # 0
# 'Conference proceedings',  # 1
# 'Journal',  # 2
# 'Work in progress',  # 3
# 'Technical report',  # 4
# 'Book',  # 5
# 'Book chapter',  # 6
# 'working paper', # 7
# 'peer-reviewed' # 8
publication_type = "3"

# Exclude publications that are shown in the Selected Publications widget?
exclude_selected = false
+++
