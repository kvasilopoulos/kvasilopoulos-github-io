+++
title = "Speculative Bubbles in Segmented Markets: Evidence from Chinese Cross-Listed Stocks"
date = 2019-06-07T00:47:09+01:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Pavlidis E.", "Vasilopoulos K."]

# Publication type.
# Legend:
# 'Uncategorized',  # 0
# 'Conference proceedings',  # 1
# 'Journal',  # 2
# 'Work in progress',  # 3
# 'Technical report',  # 4
# 'Book',  # 5
# 'Book chapter',  # 6
# 'working paper', # 7
# 'peer-reviewed' # 8

publication_types = ["7"]

# Publication name and optional abbreviated version.
publication = "" # this appears in the pop-up
publication_short = "" # this appears in the main page

# Abstract and optional shortened version.
abstract = "We propose a novel approach for testing for rational speculative bubbles in segmented capital markets. The basic idea is that, under capital controls, heterogeneity of speculative expectations across international equity markets causes financial assets with identical cash flow promises to trade at different prices. Because these deviations from the law of one price inherit the properties of the speculative bubble process, they display periods of explosive dynamics and have predictive power for future movements in equity prices in sample. These two hypotheses can be examined empirically using sequential unit root tests and predictive regressions. An attractive feature of this approach for bubble detection is that it does not require the specification of a model for market fundamentals, thus mitigating the well-known joint hypothesis problem. The focus of the paper is on mainland Chinese companies that cross list shares in Hong Kong. China is an ideal setting for our analysis because of the significant restrictions on capital movements imposed by the authorities and the turbulent behaviour of its stock market over the last decades."

abstract_short = ""

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []

# Links (optional).
url_pdf = "files/AH-Premium-working-paper.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# url_custom = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = false

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
