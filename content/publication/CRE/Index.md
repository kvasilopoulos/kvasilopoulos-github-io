+++
title = "Commercial Real Estate, Housing and the Business Cycle"
date = 2019-06-07T00:47:09+01:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Vasilopoulos K.", "Tayler W."]

# Publication type.
# Legend:
## 'Uncategorized',  # 0
# 'Conference proceedings',  # 1
# 'Journal',  # 2
# 'Work in progress',  # 3
# 'Technical report',  # 4
# 'Book',  # 5
# 'Book chapter',  # 6
# 'working paper', # 7
# 'peer-reviewed' # 8

publication_types = ["7"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = ""

# Abstract and optional shortened version.
abstract = "The 2007-08 financial crisis exposed the magnitude through which distortions in residential and commercial real estate markets can spillover to the goods markets and the real economy. This paper develops an RBC model with a consumption good and a construction sector, where both commercial and residential real estate forms the construction sector. We investigate the property-price and investment dynamics when there is competition between households and firms for real estate and the implications this market structure has for macroeconomic fluctuations. In doing so we develop a 'real estate substitution channel', where demand shocks, by raising the costs of production create a substitution between the two types of real estate. This gives an alternative interpretation to the housing preference shock, which rather than simply representing a shift in preferences, is also explained as a supply shock to the commercial real estate. The estimated model reveals that housing preference shocks explain the largest part of the variation in property prices and residential investment, while commercial real estate prices are is driven primarily by  technology shock."

abstract_short = ""

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []

# Links (optional).
url_pdf = "files/Commercial_Real_Estate.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# url_custom = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = false

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
