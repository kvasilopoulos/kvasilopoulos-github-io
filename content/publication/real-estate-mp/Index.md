+++
title = "Real Estate and Monetary Policy: Residential and Commercial"
date = 2019-06-07T00:47:09+01:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Vasilopoulos K."]

# Publication type.
# Legend:
# 'Uncategorized',  # 0
# 'Conference proceedings',  # 1
# 'Journal',  # 2
# 'Work in progress',  # 3
# 'Technical report',  # 4
# 'Book',  # 5
# 'Book chapter',  # 6
# 'working paper', # 7
# 'peer-reviewed' # 8

publication_types = ["3"]

# Publication name and optional abbreviated version.
publication = "" # this appears in the pop-up
publication_short = "" # this appears in the main page

# Abstract and optional shortened version.
abstract = "Residential and commercial real estate account for a substantial share of both household wealth and capital stock in the EU countries and the US. More specifically in the US, one-third of household net worth is held in residential real estate, whilst commercial real estate typically accounts for about half the net market (replacement) value of nonfarm, nonfinancial corporate business assets (Case2000). Furthermore, fluctuations in real estate values have been shown to play a significant and unique role both the amplification and the propagation of the business cycle, especially when combined with financial frictions in the form of collateral constraints. The general mechanism is that when households or firms use real estate as a collateral for borrowing, a decline in real estate prices will reduce the value of available collateral, and with it the availability of funds for either investment or consumption, thus amplifying the decline in economic activity (Iacoviello2005, Iacoviello2010, Liu2013, Guerrieri2017). The 2007-08 financial crisis exposed the magnitude through which distortions in residential and commercial real estate markets can spillover to the goods markets and the real economy. Real estate has long been considered as hedge against inflation (Fama1977), hence the real estate market serves as a medium for the transmission of monetary policy shocks to the overall economy. There are several channels that are at play, among which two are going to be explored here. First, the role of monetary policy in affecting the behaviour of real estate investment and property prices, as opposed to other, possibly non-fundamental factors that drive house prices up and down, such as bubbles. Second, the role of borrowing in affecting and possibly amplifying the effect of changes in property prices (in turn due to both monetary and non-monetary factors) on investment, value of collateral and overall economic activity through a financial accelerator mechanism. In our analysis, we consider both commercial and residential, therefore, it is important to disaggregate investment activity. It is households that demand housing services and consumption-good firms that are using commercial real estate to facilitate their business activities. These two combined constitute the demand for real estate. Both agents are credit-constrained and are allowed to borrow only against collateral, however, they differ in the way that they secure their financing. Firms borrow at bank prime loan rate, while households request mortgage loans at the mortgage lending rate. Both residential and commercial real estate are supplied by the construction sector. Specifically, construction firms still borrow at the same rate as consumption-good firms, however since they do not own the  real estate, they cannot pledge it as collateral. The objective of this study is to develop a framework to study the transmission of monetary policy to the real estate markets. We will carry out a structural analysis using a Structural Vector Autoregression approach (SVAR), which is conditional on the identification of a restricted number of structural shocks. Similar SVAR will be constructed for both commercial and residential real estate in order to obtain comparable results for the two markets. We first estimate the market separately, when we will focus on monetary policy, real estate demand and credit supply (borrowing) of the separate markets and compare the two impulse responses in the two markets to understand similarities and differences in a systematic manner. The second part of the research will  model the construction sector, where both types of real estate are estimated jointly. A unified approach would help us further understand the potential spillovers and comovements between the two real estate sectors."

abstract_short = ""

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# url_custom = [{name = "Custom Link", url = "http://example.org"}]

# Does this page contain LaTeX math? (true/false)
math = false

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
