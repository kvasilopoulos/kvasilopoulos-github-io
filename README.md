# Personal website

The website powered by [![Hugo](https://raw.githubusercontent.com/gohugoio/hugoDocs/master/static/img/hugo-logo.png)](https://gohugo.io/) and adapted from the [Academic theme](https://sourcethemes.com/academic/).

[![Netlify Status](https://api.netlify.com/api/v1/badges/1ce17f1a-ece5-4937-80b1-69622939ff7f/deploy-status)](https://app.netlify.com/sites/kvasilopoulos/deploys)